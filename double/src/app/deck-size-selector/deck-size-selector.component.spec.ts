import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeckSizeSelectorComponent } from './deck-size-selector.component';

describe('DeckSizeSelectorComponent', () => {
  let component: DeckSizeSelectorComponent;
  let fixture: ComponentFixture<DeckSizeSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeckSizeSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeckSizeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
