import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CardService, Even } from '../card.service';

type formStyle = 'form-horizontal' | 'form-inline';

@Component({
  selector: 'app-deck-size-selector',
  templateUrl: './deck-size-selector.component.html',
  styleUrls: ['./deck-size-selector.component.css']
})
export class DeckSizeSelectorComponent implements OnInit {

	@Input() styleClass: formStyle = 'form-inline';
	public deckForm: FormGroup = new FormGroup({});
	public even: Even;

	private formBuilder: FormBuilder = new FormBuilder();

	constructor(
		public cardService: CardService,
		private readonly router: Router
	) { }

	ngOnInit(): void {
		this.createForm();
	}

	public startGame(): void {
		this.cardService.deckSize.next(this.deckForm.value.deck);
		this.router.navigate(['deck']);
	}

	private createForm() {
		this.deckForm = this.formBuilder.group({
			deck: [(this.even) ? this.even.value : '', Validators.required]
		});
	}

}
