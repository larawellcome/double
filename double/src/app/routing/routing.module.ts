import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WelcomeComponent } from 'src/app/welcome/welcome.component';
import { PageNotFoundComponent } from 'src/app/page-not-found/page-not-found.component';
import { DeckComponent } from '../deck/deck.component';

@NgModule({
	imports: [
		RouterModule.forRoot([
			{ path: 'welcome', component: WelcomeComponent },
			{ path: 'deck', component: DeckComponent },
			{ path: '', redirectTo: 'welcome', pathMatch: 'full' },
			{ path: '**', component: PageNotFoundComponent }
		]) 
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }

