import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'double';
	public isDeckSelectorVisible: boolean = false;

	constructor(private readonly router: Router) {
		this.checkRoute();
	}

	private checkRoute(): void {
		this.router.events.subscribe(e => {
			if(e instanceof NavigationEnd){
			  this.isDeckSelectorVisible = Boolean(e.url === '/deck');
			}
		  });
	}
}
