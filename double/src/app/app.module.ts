import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app/routing/routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CardService } from 'src/app/card.service';
import { ReactiveFormsModule } from '@angular/forms';
import { DeckComponent } from './deck/deck.component';
import { DeckSizeSelectorComponent } from './deck-size-selector/deck-size-selector.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageNotFoundComponent,
    DeckComponent,
    DeckSizeSelectorComponent
  ],
  imports: [
    BrowserModule,
	BrowserAnimationsModule,
	AppRoutingModule,
	ReactiveFormsModule
  ],
  providers: [
	  CardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
