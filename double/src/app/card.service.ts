import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Even {
	id: number;
	value: 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20;
}

interface Card {
	name: string;
	selected: boolean;
	disabled?: boolean;
}

interface Img {
	id: number;
	name: string;
}

@Injectable({
	providedIn: 'root'
})
export class CardService {

	public deckSize: BehaviorSubject<number> = new BehaviorSubject<number>(0);
	public cards: Card[] = [];
	public tryings: number = 0;
	public best: number = 0;
	public disabledAllCard: boolean = false;
	public evens: Even[] = [
		{ id: 1, value: 6 },
		{ id: 2, value: 8 },
		{ id: 3, value: 10 },
		{ id: 4, value: 12 },
		{ id: 5, value: 14 },
		{ id: 6, value: 16 },
		{ id: 7, value: 18 },
		{ id: 8, value: 20 }
	];

	private images: Img[] = [
		{ id: 1, name: 'angular.png' },
		{ id: 2, name: 'd3.png' },
		{ id: 3, name: 'jenkins.png' },
		{ id: 4, name: 'postcss.png' },
		{ id: 5, name: 'react.png' },
		{ id: 6, name: 'redux.png' },
		{ id: 7, name: 'sass.png' },
		{ id: 8, name: 'splendex.png' },
		{ id: 9, name: 'ts.png' },
		{ id: 10, name: 'webpack.png' }
	];

	constructor() {
		this.deckSize.subscribe(size => {
			this.setCard(size);
		});
	}

	public selectCard(card: Card): void {
		if (this.disabledAllCard) {
			return;
		}
		++this.tryings;

		const sameCard = this.findSelectedCard(card);
		if (sameCard) {
			card.disabled = true;
			card.selected = false;
			sameCard.disabled = true;
			sameCard.selected = false;
			this.checkIfAllDisabled();
		} else {
			const selectedCards = this.cards.filter(card => card.selected === true);
			card.selected = true;
			if (selectedCards.length > 0) {
				this.turnBack(card);
			}
		}
	}

	private checkIfAllDisabled(): void {
		const disabledCards = this.cards.filter(card => card.disabled === true).length;
		if(disabledCards === this.cards.length) {
			this.best = this.tryings;
		}
	}

	private async turnBack(card: Card): Promise<void> {
		this.disabledAllCard = true;
		await this.wait();
		this.cards.map(card => card.selected = false);
		card.selected = false;
		this.disabledAllCard = false;
	}

	private wait(): Promise<any> {
		return new Promise(resolve => setTimeout(resolve, 3000));
	}

	private findSelectedCard(currentCard: Card): Card {
		return this.cards.find(card => card.selected === true && card.name === currentCard.name && card !== currentCard);
	}

	private setCard(size: number): void {
		this.cards.length = 0;
		for (let i = 0; i < (size / 2); ++i) {
			this.addCard(i);
			this.addCard(i);
		}
		this.shuffle();
	}

	private addCard(i: number) {
		this.cards.push({ name: this.images[i].name, selected: false });
	}

	private shuffle(): void {
		var currentIndex = this.cards.length, temporaryValue, randomIndex;
		while (0 !== currentIndex) {
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			temporaryValue = this.cards[currentIndex];
			this.cards[currentIndex] = this.cards[randomIndex];
			this.cards[randomIndex] = temporaryValue;
		}
	}

}
