import { Component } from '@angular/core';
import { CardService } from '../card.service';

@Component({
	selector: 'app-deck',
	templateUrl: './deck.component.html',
	styleUrls: ['./deck.component.css']
})
export class DeckComponent {

	constructor(public cardService: CardService) { }

	public restart(): void {
		this.cardService.deckSize.next(this.cardService.deckSize.value);
		this.cardService.tryings = 0;
	}

}
